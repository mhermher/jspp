import * as fs from 'fs';

export enum vType { numeric, string }

export class Column {
    private type : vType;
    private width : number;
    private name : string;
    private longName : string;
    private description : string;
    private printCode : number;
    private writeCode : number;
    private missingCodes : number[] = [];
    private missingStrings : string[] = [];
    private missingRange : number[] = [];
    private labelSet : LabelSet;
    private values : (number | string)[] = [];
    private labeled : string[] = [];
    constructor(connection : number){
        let magic : number = JSPP.readInt(connection);
        if (magic != 2){
            throw 'Variable read error. Expected : 2; Actual :' + magic;
        }
        let typeCode : number = JSPP.readInt(connection);
        if (typeCode == -1){
            throw 'Unexpected variable continuation.';
        }
        let labelFlag : number = 0;
        this.type = typeCode == 0 ? vType.numeric : vType.string;
        this.width = typeCode;
        labelFlag = JSPP.readInt(connection);
        let missingType : number = JSPP.readInt(connection);
        this.printCode = JSPP.readInt(connection);
        this.writeCode = JSPP.readInt(connection);
        this.name = JSPP.readString(connection, 8).trim();
        if (labelFlag){
            let labelLen : number = JSPP.readInt(connection);
            let readLen : number;
            if (labelLen % 4){
                readLen = labelLen + (4 - (labelLen % 4));
            } else {
                readLen = labelLen;
            }
            this.description = JSPP.readString(connection, readLen, labelLen);
        }
        for (let i : number = 0; i < missingType; i++){
            if (this.type == vType.numeric){
                this.missingCodes.push(JSPP.readReal(connection));
            } else {
                this.missingStrings.push(JSPP.readString(connection, 8));
            }
        }
        for (let i : number = 0; i > missingType; i--){
            if (this.type == vType.numeric){
                this.missingRange.push(JSPP.readReal(connection));
            } else {
                throw 'Bad missing type encoded.';
            }
        }
    }
    public getType() : vType {
        return this.type;
    }
    public getWidth() : number {
        return this.width;
    }
    public getName() : string {
        return this.name;
    }
    public setLongName(longName : string) : void {
        this.longName = longName;
    }
    public setWidth(width : number) : void {
        this.width = width;
    }
    public getLongName() : string {
        return this.longName;
    }
    public getDescription() : string {
        return this.description;
    }
    public setLabelSet(labelSet : LabelSet) : void {
        this.labelSet = labelSet;
    }
    public getLabelSet() : LabelSet {
        return this.labelSet;
    }
    public getLength() : number {
        return this.type == vType.numeric ? 1 : Math.ceil(this.width / 8);
    }
    public pushValue(value : (number | string)) : void {
        this.values.push(value);
        if (this.labelSet){
            if (value === null){
                this.labeled.push(null);
                return;
            }
            let matched : boolean = false;
            for (let i : number = 0; i < this.labelSet.getMaps().length; i++){
                if (value == this.labelSet.getMaps()[i].level){
                    matched = true;
                    this.labeled.push(this.labelSet.getMaps()[i].label);
                    break;
                }
            }
            if (!matched){
                this.labeled.push(undefined);
            }
            return;
        }
    }
    public getValues(labeled : boolean = false){
        if (labeled && this.labelSet){
            return this.labeled;
        } else {
            return this.values;
        }
    }
}

export class LabelSet {
    private maps : {
        level : number,
        label : string
    }[] = [];
    private indices : number[] = [];
    constructor(connection : number){
        let count : number = JSPP.readInt(connection);
        let labelLen : number;
        let readLen : number;
        let level : number;
        for (let i : number = 0; i < count; i++){
            level = JSPP.readReal(connection);
            labelLen = JSPP.readByte(connection);
            if ((labelLen + 1) % 8){
                readLen = labelLen + (8 - ((labelLen + 1) % 8))
            } else {
                readLen = labelLen;
            }
            this.maps.push({
                level : level,
                label : JSPP.readString(connection, readLen, labelLen)
            });
        }
        let magic : number = JSPP.readInt(connection);
        if (magic != 4){
            throw 'Labels reading error. Expected : 4; Actual :' + magic;
        }
        count = JSPP.readInt(connection);
        for (let i : number = 0; i < count; i++){
            this.indices.push(JSPP.readInt(connection));
        }
    }
    public getMaps() : {level : number, label : string}[] {
        return this.maps;
    }
    public getIndices() : number[] {
        return this.indices;
    }

}

class CaseReader {
    private instructions : Buffer = new Buffer(8);
    private cursor : number = 8;
    private connection : number;
    private bias : number;
    private instruct() : number {
        if (this.cursor > 7){
            this.cursor = 0;
            fs.readSync(this.connection, this.instructions, 0, 8, null);
        }
        return(this.instructions[this.cursor++]);
    }
    public nextNumeric() : number {
        let instruction : number;
        do {
            instruction = this.instruct();
        } while (instruction == 0)
        switch (instruction){
            case 252 :
                throw 'Unexpected end of records.';
            case 253 :
                return JSPP.readReal(this.connection);
            case 254 :
                throw 'Code 254 does not match numeric type.';
            case 255 :
                return null;
            default :
                return instruction - this.bias;
        }
    }
    public nextString(length : number = 1) : string {
        let instruction : number;
        do {
            instruction = this.instruct();
        } while (instruction == 0)
        switch (instruction){
            case 252 :
                throw 'Unexpected end of records.';
            case 253 :
                if (--length){
                    return JSPP.readString(this.connection, 8).concat(this.nextString(length));
                } else {
                    return JSPP.readString(this.connection, 8);
                }
            case 254 :
                if (--length){
                    return this.nextString(length);
                } else {
                    return '';
                }
            case 255 :
                return null
            default :
                throw 'Default codes not supported for string types.'
        }
    }
    constructor(connection : number, bias : number) {
        this.connection = connection;
        this.bias = bias;
    }
}

export interface headerInfo {
    product : string,
    layout : number,
    variables : number,
    compression : number,
    weightIndex : number,
    cases : number,
    bias : number,
    createdDate : string,
    createdTime : string,
    label : string
}

export interface systemInfo {
    float : {
        missing : number,
        high : number,
        low : number
    },
    integer : {
        major : number,
        minor : number,
        revision : number,
        machine : number,
        float : number,
        compression : number,
        endianness : number,
        character : number
    },
    display : number[]
}

export class JSPP {
    private static charsets : {code : number, name : string}[] = [
        { code : 1, name : "EBCDIC-US" },
        { code : 2, name : "WINDOWS-1252" },
        { code : 3, name : "WINDOWS-1252" },
        { code : 4, name : "DEC-KANJI" },
        { code : 437, name : "CP437" },
        { code : 708, name : "ASMO-708" },
        { code : 737, name : "CP737" },
        { code : 775, name : "CP775" },
        { code : 850, name : "CP850" },
        { code : 852, name : "CP852" },
        { code : 855, name : "CP855" },
        { code : 857, name : "CP857" },
        { code : 858, name : "CP858" },
        { code : 860, name : "CP860" },
        { code : 861, name : "CP861" },
        { code : 862, name : "CP862" },
        { code : 863, name : "CP863" },
        { code : 864, name : "CP864" },
        { code : 865, name : "CP865" },
        { code : 866, name : "CP866" },
        { code : 869, name : "CP869" },
        { code : 874, name : "CP874" },
        { code : 932, name : "SHIFT-JIS" },
        { code : 936, name : "ISO-IR-58" },
        { code : 949, name : "ISO-IR-149" },
        { code : 950, name : "BIG-5" },
        { code : 1200, name : "UTF-16LE" },
        { code : 1201, name : "UTF-16BE" },
        { code : 1250, name : "WINDOWS-1250" },
        { code : 1251, name : "WINDOWS-1251" },
        { code : 1252, name : "WINDOWS-1252" },
        { code : 1253, name : "WINDOWS-1253" },
        { code : 1254, name : "WINDOWS-1254" },
        { code : 1255, name : "WINDOWS-1255" },
        { code : 1256, name : "WINDOWS-1256" },
        { code : 1257, name : "WINDOWS-1257" },
        { code : 1258, name : "WINDOWS-1258" },
        { code : 1361, name : "CP1361" },
        { code : 10000, name : "MACROMAN" },
        { code : 10004, name : "MACARABIC" },
        { code : 10005, name : "MACHEBREW" },
        { code : 10006, name : "MACGREEK" },
        { code : 10007, name : "MACCYRILLIC" },
        { code : 10010, name : "MACROMANIA" },
        { code : 10017, name : "MACUKRAINE" },
        { code : 10021, name : "MACTHAI" },
        { code : 10029, name : "MACCENTRALEUROPE" },
        { code : 10079, name : "MACICELAND" },
        { code : 10081, name : "MACTURKISH" },
        { code : 10082, name : "MACCROATIAN" },
        { code : 12000, name : "UTF-32LE" },
        { code : 12001, name : "UTF-32BE" },
        { code : 20127, name : "US-ASCII" },
        { code : 20866, name : "KOI8-R" },
        { code : 20932, name : "EUC-JP" },
        { code : 21866, name : "KOI8-U" },
        { code : 28591, name : "ISO-8859-1" },
        { code : 28592, name : "ISO-8859-2" },
        { code : 28593, name : "ISO-8859-3" },
        { code : 28594, name : "ISO-8859-4" },
        { code : 28595, name : "ISO-8859-5" },
        { code : 28596, name : "ISO-8859-6" },
        { code : 28597, name : "ISO-8859-7" },
        { code : 28598, name : "ISO-8859-8" },
        { code : 28599, name : "ISO-8859-9" },
        { code : 28603, name : "ISO-8859-13" },
        { code : 28605, name : "ISO-8859-15" },
        { code : 50220, name : "ISO-2022-JP" },
        { code : 50221, name : "ISO-2022-JP" },
        { code : 50222, name : "ISO-2022-JP" },
        { code : 50225, name : "ISO-2022-KR" },
        { code : 50229, name : "ISO-2022-CN" },
        { code : 51932, name : "EUC-JP" },
        { code : 51936, name : "GBK" },
        { code : 51949, name : "EUC-KR" },
        { code : 52936, name : "HZ-GB-2312" },
        { code : 54936, name : "GB18030" },
        { code : 65000, name : "UTF-7" },
        { code : 65001, name : "UTF-8" }
    ]
    private static readRaw(connection : number, size : number) : Buffer {
        let buf = new Buffer(size);
        if (fs.readSync(connection, buf, 0, size, null) != size){
            throw 'Unexpected end of file.'
        }
        return(buf);
    }
    public static readString(connection : number, size : number, trim : number = size) : string {
        return JSPP.readRaw(connection, size).toString().substring(0, trim);
    }
    public static readInt(connection : number) : number {
        return JSPP.readRaw(connection, 4).readInt32LE(0);
    }
    public static readReal(connection : number) : number {
        return JSPP.readRaw(connection, 8).readDoubleLE(0);
    }
    public static readByte(connection : number) : number {
        return JSPP.readRaw(connection, 1).readInt8(0);
    }
    private static expectContinue(connection : number) : void {
        let magic : number = JSPP.readInt(connection);
        if (magic != 2){
            throw 'Variable read error. Expected : 2; Actual : ' + magic;
        }
        let typeCode : number = JSPP.readInt(connection);
        if (typeCode == -1){
            JSPP.readString(connection, 24);
        } else {
            throw 'Variable read error. Expected continuation.';
        }
    }
    private static readDocument(connection : number) : string[] {
        let count : number = JSPP.readInt(connection);
        let docu : string[] = [];
        for (let i : number = 0; i < count; i++){
            docu.push(JSPP.readString(connection, 80));
        }
        return(docu);
    }
    protected header : headerInfo;
    protected system : systemInfo = {
        float : {
            missing : null,
            high : null,
            low : null
        },
        integer : {
            major : null,
            minor : null,
            revision : null,
            machine : null,
            float : null,
            compression : null,
            endianness : null,
            character : null
        },
        display : []
    }
    protected columns : Column[] = [];
    protected documents : string[][] = [];
    protected labelSets : LabelSet[] = [];
    protected varHash : {[key : string] : number} = {};
    constructor(path : string){
        let con : number = fs.openSync(path, 'r')
        if (!con){
            throw 'Can not read from path.';
        }
        let buf = new Buffer(4);
        fs.readSync(con, buf, 0, 4, null);
        if (buf.toString() != "$FL2"){
            throw 'File is not sav. Expected : $FL2; Actual : ' + buf.toString();
        }
        this.header = {
            product : JSPP.readString(con, 60),
            layout : JSPP.readInt(con),
            variables : JSPP.readInt(con),
            compression : JSPP.readInt(con),
            weightIndex : JSPP.readInt(con),
            cases : JSPP.readInt(con),
            bias : JSPP.readReal(con),
            createdDate : JSPP.readString(con, 9),
            createdTime : JSPP.readString(con, 8),
            label : JSPP.readString(con, 64)
        };
        JSPP.readString(con, 3); // padding
        let contCount : number = 0;
        let nextVar : Column;
        let varMap : number[] = [];
        for (let i : number = 0; i < this.header.variables; i++){
            if (contCount){
                varMap.push(undefined);
                JSPP.expectContinue(con);
                contCount--;
            } else {
                nextVar = new Column(con);
                this.varHash[nextVar.getName()] = this.columns.length;
                varMap.push(this.columns.length);
                this.columns.push(nextVar);
                contCount = nextVar.getLength() - 1;
            }
        }
        let nextCode : number;
        let keepGoing : boolean = true;
        let subCode : number;
        let dataLen : number;
        let dataCnt : number;
        let rawNames : string[][];
        let rawLengths : string[][];
        let labelSet : LabelSet;
        let varIdx : number;
        let readWidth : number;
        let writeWidth : number;
        let varMatch : number;
        let varCount : number;
        let spliceouts : number[][] = [];
        let offset : number = -1;
        while (keepGoing){
            nextCode = JSPP.readInt(con);
            switch (nextCode){
                case 3 :
                    labelSet = new LabelSet(con);
                    for (let i : number = 0; i < labelSet.getIndices().length; i++){
                        varIdx = varMap[labelSet.getIndices()[i] - 1];
                        if (varIdx === undefined){
                            throw 'Value labels associated with continuous variable slots.';
                        }
                        this.columns[varIdx].setLabelSet(labelSet);
                    }
                    this.labelSets.push(labelSet);
                    break;
                case 6 :
                    this.documents.push(JSPP.readDocument(con));
                    break;
                case 7 :
                    subCode = JSPP.readInt(con);
                    dataLen = JSPP.readInt(con);
                    dataCnt = JSPP.readInt(con);
                    switch (subCode){
                        case 3 :
                            if (dataLen * dataCnt != 32){
                                throw 'Expected 32 byte sized for integer info.';
                            }
                            this.system.integer = {
                                major : JSPP.readInt(con),
                                minor : JSPP.readInt(con),
                                revision : JSPP.readInt(con),
                                machine : JSPP.readInt(con),
                                float : JSPP.readInt(con),
                                compression : JSPP.readInt(con),
                                endianness : JSPP.readInt(con),
                                character : JSPP.readInt(con)
                            };
                            break;
                        case 4 :
                            if (dataLen * dataCnt != 24){
                                throw 'Expected 24 byte sized for float info.';
                            }
                            this.system.float = {
                                missing : JSPP.readReal(con),
                                high : JSPP.readReal(con),
                                low : JSPP.readReal(con)
                            };
                            break;
                        case 11 :
                            if (dataLen != 4){
                                throw 'Expected 4 byte sized for display info.';
                            }
                            for (let i : number = 0; i < dataCnt; i++){
                                this.system.display.push(JSPP.readInt(con));
                            }
                            break;
                        case 13 :
                            rawNames = JSPP.readString(con, dataLen * dataCnt).split('\t').map(
                                str => str.split('=')
                            );
                            for (let i : number = 0; i < rawNames.length; i++){
                                this.columns[this.varHash[rawNames[i][0]]].setLongName(
                                    rawNames[i][1]
                                );
                            }
                            break;
                        case 14 :
                            //very long strings
                            rawLengths = JSPP.readString(con, dataLen * dataCnt).split('\t').map(
                                str => str.split('=')
                            );
                            rawLengths = rawLengths.slice(0, rawLengths.length - 1);
                            for (let i : number = 0; i < rawLengths.length; i++){
                                varMatch = this.varHash[rawLengths[i][0]];
                                varCount = 0;
                                writeWidth = 0;
                                readWidth = parseInt(rawLengths[i][1]);
                                while (readWidth > 0){
                                    readWidth -= this.columns[varMatch].getWidth();
                                    writeWidth += this.columns[varMatch++].getLength()*8;
                                    varCount++;
                                }
                                this.columns[this.varHash[rawLengths[i][0]]].setWidth(
                                    writeWidth
                                );
                                spliceouts.push([
                                    this.varHash[rawLengths[i][0]] - offset,
                                    varCount - 1
                                ]);
                                offset += (varCount - 1);
                            }
                            break;
                        case 21 :
                            //TODO: long value labels record
                            JSPP.readString(con, dataLen * dataCnt);
                        default :
                            JSPP.readString(con, dataLen * dataCnt);
                    }
                    break;
                case 999 :
                    JSPP.readString(con, 4); //padding
                    keepGoing = false;
                    break;
                default :
                    throw 'Dictionary error read. Expected : [3, 6, 7, 999]; Actual : ' + nextCode;
            }
        }
        for (let i : number = 0; i < spliceouts.length; i++){
            this.columns.splice(spliceouts[i][0], spliceouts[i][1]);
        }
        if (!this.header.compression){
            throw 'Uncompressed data file reads not supported yet.';
        }
        let caseReader = new CaseReader(con, this.header.bias);
        for (let i : number = 0; i < this.header.cases; i++){
            for (let j : number = 0; j < this.columns.length; j++){
                switch (this.columns[j].getType()) {
                    case vType.numeric :
                        this.columns[j].pushValue(caseReader.nextNumeric());
                        break;
                    case vType.string :
                        this.columns[j].pushValue(
                            caseReader.nextString(this.columns[j].getLength())
                        );
                        break;
                    default :
                        throw 'Unexpected variable type information.'
                }
            }
        }
    }
    public getData(labeled : boolean = false, colwise : boolean = true) : any {
        let colData : any = {};
        let rowData : any = Array(this.header.cases).fill({});
        for (let i : number = 0; i < this.columns.length; i++){
            colData[this.columns[i].getLongName()] = this.columns[i].getValues(labeled);
        }
        if (colwise){
            return colData;
        }
        for (let i : number = 0; i < this.columns.length; i++){
            for (let j : number = 0; j < this.header.cases; j++){
                rowData[j][this.columns[i].getLongName()] = colData[this.columns[i].getLongName()][j];
            }
        }
        return rowData;
    }
}